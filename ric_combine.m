function [out] = ric_combine(shown, hidden, r)

%resize shit
shown = ric_resize(shown, hidden);
hidden = ric_resize(hidden, shown);
hidden = imresize(hidden, 1/2);
shown = imresize(shown, r);
hidden = imresize(hidden, r);
%modify colors
hidden = ceil(hidden/16)+238;
shown = ceil(shown/16*12);

%combine pictures
shown(1:2:size(shown, 1), 1:2:size(shown, 2), :) = hidden;
out = shown;
end

